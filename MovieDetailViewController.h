//
//  MovieDetailViewController.h
//  MyMovies
//
//  Created by Mehmet Can Yavuz on 08.02.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyMovies.h"
@interface MovieDetailViewController : UIViewController
- (IBAction)saveMovie:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *poster;
@property (strong, nonatomic) IBOutlet UILabel *movieName;
@property (strong, nonatomic) IBOutlet UILabel *year;
@property (strong, nonatomic) IBOutlet UILabel *director;
@property (nonatomic, retain) Movie * movie;
@end
