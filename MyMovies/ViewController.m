//
//  ViewController.m
//  MyMovies
//
//  Created by Mehmet Can Yavuz on 05.02.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import "ViewController.h"
#import "UIImageView+WebCache.h"
#import "HandsomeManager.h"
#import "MovieCell.h"
#import "MovieDetailViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.saveMovieButton.hidden = YES;
   // self.moviesTable.editing = YES;
    [self.moviesTable registerNib:[UINib nibWithNibName:@"MovieCell" bundle:[NSBundle mainBundle]]
           forCellReuseIdentifier:@"MovieCell"];
    self.title = @" My Movies ";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveMovie:(id)sender {
  //  [self.movie save];
//    [self.movie saveWithCallback:^(){
//      
//        NSLog(@"saved");
//    }
//     ];
   
    //[self.movie saveWithTarget:self andSelector:@selector(saveSuccessful)];
}

-(void) saveSuccessful {
    NSLog(@"Saved");

}
- (IBAction)getMovie:(id)sender {
    [self.imdbID resignFirstResponder];
    GetMovieRequest * request = [[GetMovieRequest alloc] initWithDelegate:self];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
   NSString * text =  [self.imdbID.text stringByReplacingOccurrencesOfString:@" " withString:@"+" ];
    request.movieName = text;
    request.successBlock = ^{
        GetMovieResponse * response = [[GetMovieResponse alloc] init];
        response = (GetMovieResponse * ) request.response;
        self.movies = response.movies;
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [self.moviesTable reloadData];

    };
    [request run];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.movies count];
}

-(UITableViewCell *)tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MovieCell *aCell = (MovieCell *)[tableView dequeueReusableCellWithIdentifier:@"MovieCell"];
    
    if(aCell == nil) {
        UIViewController *temp = [[UIViewController alloc]initWithNibName:@"MovieCell" bundle:nil];
        aCell = (MovieCell *)temp.view;
        
    }
    Movie * m = [self.movies objectAtIndex:indexPath.row];
    [aCell setMovie:m];
    return aCell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath  {
    
   MovieDetailViewController *md = [[MovieDetailViewController alloc] initWithNibName:@"MovieDetailViewController" bundle:nil];
    md.movie    = [self.movies objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:md animated:YES];
    
}


@end
