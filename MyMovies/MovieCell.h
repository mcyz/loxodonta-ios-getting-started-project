//
//  MovieCell.h
//  MyMovies
//
//  Created by Mehmet Can Yavuz on 06.02.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyMovies.h"
@interface MovieCell : UITableViewCell
-(void) setMovie:(Movie*)movie;
@property (nonatomic, retain) IBOutlet UILabel *movieName;
@property (nonatomic, retain) IBOutlet UILabel *director;
@property (nonatomic, retain) IBOutlet UILabel *year;
@property (nonatomic, retain) IBOutlet UIImageView *poster;
@end
