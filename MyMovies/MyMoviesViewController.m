//
//  MyMoviesViewController.m
//  MyMovies
//
//  Created by Mehmet Can Yavuz on 05.02.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import "MyMoviesViewController.h"
#import "HandsomeManager.h"
#import "MyMovies.h"
#import "MovieCell.h"
@interface MyMoviesViewController ()

@end

@implementation MyMoviesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
          self.title = @"List Movies";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.moviesTable registerNib:[UINib nibWithNibName:@"MovieCell" bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"MovieCell"];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)listMyMovies:(id)sender{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [Movie listHandsomeObjects:@"Movie" complete:^(NSArray * arr){
        self.dataArr = arr;
         [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [self.moviesTable reloadData];

    }
     ];
}


-(void) listFetched: (NSMutableArray *) arr {
    NSLog(@"Movies listed");
    NSLog(@"%@",self.dataArr);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.dataArr count];
}

-(UITableViewCell *)tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MovieCell *aCell = (MovieCell *)[tableView dequeueReusableCellWithIdentifier:@"MovieCell"];
    
    if(aCell == nil) {
        UIViewController *temp = [[UIViewController alloc]initWithNibName:@"MovieCell" bundle:nil];
        aCell = (MovieCell *)temp.view;
       
    }
    Movie * m = [self.dataArr objectAtIndex:indexPath.row];
    [aCell setMovie:m];
    return aCell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
   
    Movie * m = [self.dataArr objectAtIndex:indexPath.row];
     [self.dataArr removeObjectAtIndex:indexPath.row];
    [m removeWithTarget:self andSelector:@selector(removeSuccess)];
    [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];

   

}

-(void) removeSuccess {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Removed" message:@"success" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
    [alert show];
}
@end
