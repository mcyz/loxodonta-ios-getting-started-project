//
//  ViewController.h
//  MyMovies
//
//  Created by Mehmet Can Yavuz on 05.02.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyMovies.h"
@interface ViewController : UIViewController <HandsomeRequestDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, retain) NSMutableArray *movies;
@property (strong, nonatomic) IBOutlet UILabel *movieName;
@property (strong, nonatomic) IBOutlet UILabel *director;
@property (strong, nonatomic) IBOutlet UIImageView *poster;
@property (strong, nonatomic) IBOutlet UITextField *imdbID;
@property (strong, nonatomic) IBOutlet UIButton *getMovieButton;
@property (strong, nonatomic) IBOutlet UILabel *year;
@property (strong, nonatomic) IBOutlet UIButton *saveMovieButton;
@property (strong, nonatomic) IBOutlet UITableView *moviesTable;

- (IBAction)saveMovie:(id)sender;
- (IBAction)getMovie:(id)sender;
@end
