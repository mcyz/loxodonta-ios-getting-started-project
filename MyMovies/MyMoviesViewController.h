//
//  MyMoviesViewController.h
//  MyMovies
//
//  Created by Mehmet Can Yavuz on 05.02.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyMovies.h"
@interface MyMoviesViewController : UIViewController
@property (nonatomic, retain) NSMutableArray * dataArr;
@property (nonatomic, retain) IBOutlet UITableView * moviesTable;

@end
