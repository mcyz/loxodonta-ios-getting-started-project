//
//  MovieCell.m
//  MyMovies
//
//  Created by Mehmet Can Yavuz on 06.02.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import "MovieCell.h"
#import "MyMovies.h"
#import "UIImageView+WebCache.h"
@implementation MovieCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setMovie:(Movie*)movie{
    
    self.movieName.text = movie.movieName;
    self.year.text = [NSString stringWithFormat:@"%d",movie.year];
    [self.poster setImageWithURL:[NSURL URLWithString:movie.poster]];
    self.director.text = [movie.directors objectAtIndex:0];
}
@end
