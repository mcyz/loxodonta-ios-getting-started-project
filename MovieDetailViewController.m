//
//  MovieDetailViewController.m
//  MyMovies
//
//  Created by Mehmet Can Yavuz on 08.02.2013.
//  Copyright (c) 2013 Mehmet Can Yavuz. All rights reserved.
//

#import "MovieDetailViewController.h"
#import "HandsomeManager.h"
#import "UIImageView+WebCache.h"
@interface MovieDetailViewController ()

@end

@implementation MovieDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.movieName.text = self.movie.movieName;
    self.year.text = [NSString stringWithFormat:@"%d",self.movie.year];
    [self.poster setImageWithURL:[NSURL URLWithString:self.movie.poster]];
    self.director.text = [self.movie.directors objectAtIndex:0];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveMovie:(id)sender {

    [self.movie saveWithTarget:self andSelector:@selector(saveSuccessful)];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

-(void)  saveSuccessful {
    NSLog(@"saved");
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Saved" message:@"success" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
    [alert show];
}
- (void)viewDidUnload {
    [self setMovieName:nil];
    [self setYear:nil];
    [self setDirector:nil];
    [super viewDidUnload];
}
@end
